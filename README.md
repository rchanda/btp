# README #


**How to Run the Code:**

Parameters for running the code:

master numIterations numPartitions numFeatures PathToTrainingFile PathToTestFile PathToOutputFile

Example Commands:
spark://dml2:7077 100 10 123 /hadoop/a1a-train /hadoop/a1a-test /home/csedept/output

local[4] 100 10 123 /home/chanda/training/a1a-train /home/chanda/test/a1a-test /home/csedept/output

**admm-gsmo branch has the latest code**

* Quick summary
* BTP Project :Implementation of ADMM with GSMO 

* Version
1.0

## **Integrating Spark with Eclipse using Scala Maven Project:** ##

1. **Install Maven** 
Download http://maven.apache.org/download.cgi
Install: http://maven.apache.org/download.cgi#Installation

2. **Install Eclipse Scala IDE**
http://scala-ide.org/download/sdk.html  Scala 2.10.4

3. **Install m2Eclipse scala plugin:** (If not present, since it is already included in the above ide.)
http://scala-ide.org/docs/tutorials/m2eclipse/
http://scala-ide.org/docs/tutorials/index.html

4. Edit pom.xml to include all dependencies 

5. Run mvn package from the project folder 
mvn package

6. Run jar using spark-submit script from spark home directory (cd /opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/bin/)
Example Command:
./spark-submit --class "com.btp.admm.SVMWithADMM" --master spark://dml2:7077 /home/chanda/git/btp/Workspace/admm/target/admm-module-1.0-SNAPSHOT-jar-with-dependencies.jar spark://dml2:7077 10 123 /home/chanda/training/a1a-train /home/chanda/test/a1a-test

**Git Commands:**

https://confluence.atlassian.com/display/STASH/Basic+Git+commands

git clone <git-clone-address-of-btp-from-your-account>
Eg: git clone https://chandar@bitbucket.org/chandar/btp.git
git checkout admm-gsmo

To get the latest code:
git checkout -- <file>          -- to discard the changes
git pull                        -- To get the latest code