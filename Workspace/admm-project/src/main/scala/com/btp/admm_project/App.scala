package com.btp.admm_project

/**
 * @author ${user.name}
 */
/*** SimpleApp.scala ***/
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._

object App {
  def main(args: Array[String]) {
    val logFile = "/home/chanda/train.txt" // Should be some file on your system
    val sc = new SparkContext("local", "Simple App", "/opt/cloudera/parcels/SPARK/lib/spark",
      List("target/scala-2.10/admm-project_2.10-1.0.jar"))
    val logData = sc.textFile(logFile, 2).cache()
    val numAs = logData.filter(line => line.contains("a")).count()
    val numBs = logData.filter(line => line.contains("b")).count()
    println("Lines with a: %s, Lines with b: %s".format(numAs, numBs))
  }
}