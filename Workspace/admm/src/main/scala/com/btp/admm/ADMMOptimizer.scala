package com.btp.admm


import org.apache.spark.AccumulableParam
import org.apache.spark.rdd.RDD
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix
import no.uib.cipr.matrix.sparse.SparseVector
import org.apache.spark.mllib.classification._
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import scala.util.control.Breaks

object WsAccumulableParam extends AccumulableParam[FlexCompRowMatrix,SparseVector] {

	/*
	 * Return the "zero" (identity) value for an accumulator type, given its initial value.
	 * For example, if R was a vector of N dimensions, this would return a vector of N zeroes.
	 */

	def zero(initialValue:FlexCompRowMatrix):FlexCompRowMatrix = {
		return initialValue

	}

	/*
	 * Merge two accumulated values together. Is allowed to modify and return the first value for efficiency (to avoid allocating objects).
	 * r1 - one set of accumulated data
	 * r2 - another set of accumulated data
	 * returns both data sets merged together 
	 * 
	 */

	def addInPlace(r1: FlexCompRowMatrix, r2: FlexCompRowMatrix) : FlexCompRowMatrix = {
		
	    val res = r1.add(r2)
		val r = new FlexCompRowMatrix(res)
	    return r
	}

	/*
	 * Add additional data to the accumulator value. Is allowed to modify and return r for efficiency (to avoid allocating objects).
	 * r - the current value of the accumulator
	 * t - the data to be added to the accumulator
	 * returns the new value of the accumulator 
	 */

	def addAccumulator(r:FlexCompRowMatrix,t:SparseVector): FlexCompRowMatrix = {
		val res = r
		res.setRow(r.numRows(),t)
		return res
	}

}

class ADMMOptimizer extends Serializable {

	private var numIterations = 1
	private var lambda = 0.01
	private var rho = 0.1
	
	def setNumIterations(iters: Int): this.type = {
		this.numIterations = iters
		this
	}
	
	def setLambda(lambda:Double): this.type = {
	  this.lambda = lambda 
	  this
	}
	
	def setRho(rho:Double): this.type = {
	  this.rho =  rho
	  this
	}
	
	/*
	Use the updated Ws from the last iteration to update Z, U
	All ops are DenseVector operations to perform:
		Z[i]=(W[i]+U[i])/(M+1/lambda*rho)
		and
		U[i]=U[i]+W[i]-Z[i]
	*/
	def update_ZU(Ws:FlexCompRowMatrix,Us:FlexCompRowMatrix):Double = {
	
		var i = 0 
		var numPartitions = Ws.numRows()
		var numFeatures = Ws.numColumns()
		
		var Z = new SparseVector(numFeatures)
		
		
		for(i <- 0 to numPartitions){
			Z.add(Ws.getRow(i));
			Z.add(Us.getRow(i));
		}
		
		Z.scale(1.0/(numPartitions+1/lambda*rho));
		
		//thresh_val: to store the standard square error between local and global weights
		val thresh = new SparseVector(numFeatures);
		var thresh_val = 0.0
		
		for(i <- 0 to numPartitions){
			Us.getRow(i).add(1,Ws.getRow(i));
			Us.getRow(i).add(-1,Z);
			
			//SparseVector ops to add ||Wi-Z||^2
			thresh.set(1,Z);
			thresh.add(-1,Ws.getRow(i));
			thresh_val = thresh_val + thresh.dot(thresh);
		}
		
		return thresh_val;
	}
	
	def optimize(data:RDD[Node],numFeatures:Int) : SparseVector = {

		val numExamples = data.count().toInt

		var Us = new FlexCompRowMatrix(numIterations,numFeatures+1)
		var Z  = new SparseVector(numFeatures) 
		var W  = new SparseVector(numFeatures)
		
		val loop = new Breaks;
        loop.breakable {
          
			for(k <- 1 until numIterations)
			{
				val initialValue =  new FlexCompRowMatrix(0,0)
				val WsAccumulator = data.context.accumulable(initialValue)(WsAccumulableParam)
	
				// Map function
				def node_process(index:Int,iter: Iterator[Node]): Iterator[Node] = {
					
					 W = iter.next.talk(Z,Us.getRow(index))
				
					 WsAccumulator.add(W)
					 return iter
				}
	
				val array = data.mapPartitionsWithIndex(node_process)
	
				println("Count = "+array.count)
				
				val thresh = update_ZU(WsAccumulator.value,Us)
				
				if(thresh <= 0.01) loop.break
				
				println("Accumulator Length "+WsAccumulator.value)
	
			}
        }
		return W
	}
}
