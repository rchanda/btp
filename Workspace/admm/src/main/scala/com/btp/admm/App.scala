package com.btp.admm

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import no.uib.cipr.matrix.sparse.SparseVector
import org.apache.spark.storage.StorageLevel
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.serializer.KryoRegistrator
import com.esotericsoftware.kryo.Kryo

class MyRegistrator extends KryoRegistrator {
  override def registerClasses(kryo: Kryo) {
    kryo.register(classOf[no.uib.cipr.matrix.sparse.FlexCompRowMatrix])
  }
}

object App {

    def main(args: Array[String]) {

		val minPartitions: Int = 3 		//Number of data points in each partition

		val conf = new SparkConf().setMaster("local[4]").setAppName("Simple ADMM Application")
		conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
		conf.set("spark.kryo.registrator", "com.btp.admm.MyRegistrator")

		val sc = new SparkContext(conf)
		
		val numIterations = 2
		
		val data = SVMWithADMM.loadLibSVMFiletoNodes(sc,"/home/chanda/examples/a1a",minPartitions).cache
		
		// Split data into training (60%) and test (40%).
		val splits = data.randomSplit(Array(0.6, 0.4), seed = 11L)
		val training = splits(0).cache()
		val test = splits(1)

		val model = SVMWithADMM.train(training)

		// Compute raw scores on the test set. 
		val scoreAndLabels = model.predict(test)
		
		// Get evaluation metrics.
		val metrics = new BinaryClassificationMetrics(scoreAndLabels)
		val auROC = metrics.areaUnderROC()
		
		println("Area under ROC = " + auROC)

	}
}