package com.btp.admm;

import java.io.*;
import java.util.*;
import java.awt.Point;
import com.google.common.cache.*;
import no.uib.cipr.matrix.sparse.*;
import no.uib.cipr.matrix.*;

public class GSMO
{
	int n, m; // n --> no of features, m --> no of examples
	int[] Y; // array to store the labels. 
	FlexCompRowMatrix _X; // FlexCompRowMatrix is a member(SparseMatrix) of Matrix Toolkit Java library, used to store the features. 
	
	double[] _a, _b, p;	//_a, _b are the range of alpha. 
	double[] alpha, D, Qdiag; // D is used to store [Qalpha]i + p_transpose, Qdiag is used to store the diagonal elements of the Q matrix. 
	double tau, b; // tau is the tolerance, b is from the equation y = w.x - b
	
	long cache_size=1000;	//defining the cache size. 
	LoadingCache<Integer, double[]> cache;
	Kernel k; // k is an object implementing the Kernel interface 
	
	double Fmin=Double.POSITIVE_INFINITY, Fmax=-Double.POSITIVE_INFINITY; // F is the gradient information
	
	
	/*
	below there are a number of constructors, to be used appropriately according to the parameters
	the user wants to pass himself. 
	The parameters as passed when the GSMO object is created from the Node class or on an individual file with data
	a0,b0 = range of alpha. tol = tolerance, ker is the kernel object the user wants to pass on depending on the type of kernel he wants to use. 
	*/

	public GSMO(FlexCompRowMatrix X, int[] Y, int m, int n, double a0,double b0, double[] p0, double tol, Kernel ker) throws IOException{
		this._X=X; this.Y=Y;
		this.m=m; this.n=n;
		_a=new double[m];
		_b=new double[m];
		Arrays.fill(_a,a0);
		Arrays.fill(_b,b0);
		p=p0;
		init(ker, tol);
	}
	
	//if no Kernel is passed, then it is assumed to be linear. 
	public GSMO(FlexCompRowMatrix X, int[] Y, int m, int n, double a0,double b0, double p0, double tol) throws IOException{
		this._X=X; this.Y=Y;
		this.m=m; this.n=n;
		_a=new double[m];
		_b=new double[m];
		p=new double[m];
		Arrays.fill(_a,a0);
		Arrays.fill(_b,b0);
		Arrays.fill(p,p0);
		
		Kernel ker = new Kernel(0.01);
		init(ker,tol);
	}
	
	public GSMO(String file, double[] a0,double[] b0,double[] p0, double tol, Kernel ker) throws IOException{ 
		parseDat(file); 
		_a=a0;	_b=b0;	p =p0;
		init(ker, tol);
	}

	public GSMO(String file, double a0, double b0, double p0, double tol, Kernel ker) throws IOException{
		parseDat(file);
		_a=new double[m];
		_b=new double[m];
		p =new double[m];
		Arrays.fill(_a,a0);
		Arrays.fill(_b,b0);
		Arrays.fill(p,p0);
		init(ker, tol);
	}
	/*
	Initialize all vectors to zeros and set the kernel and tolerance for the GSMO module
	*/
	void init(Kernel ker, double tol){ 
		k=ker;
		tau=tol;
		alpha=new double[m];
		Qdiag=new double[m];
		D=new double[m];
		for(int i=1; i<m; i++){
			//calculate diagonal elements. These are referenced repeatedly in WSS2, so they are stored
			Qdiag[i]=k.getKerVal(_X.getRow(i),_X.getRow(i),Y[i],Y[i]); 
			
			D[i]=p[i];
		}
		
		/*
		create a guava cache of size cache_size
		The CacheLoader overrides a load method that uses code supplied to fill the cache for index i
		if it is not already present automatically.
		The load method below calculates The kernal value from the Kernel k of the GSMO module for an entire row i
		and stores it in the cache.
		
		The cache implements a LRU policy for deletion
		*/		
		cache = CacheBuilder.newBuilder()
			.maximumSize(cache_size)
			.build(
				new CacheLoader<Integer, double[]>(){ 
					public double[] load(Integer i){
						double[] Qi=new double[m]; 
						for(int j=0; j<m; j++)
							Qi[j]=k.getKerVal(_X.getRow(i),_X.getRow(j),Y[i],Y[j]); 
						return Qi;
					}
				}); 
	}
	
	
	/*
	This method is called to parse the data from the data file.
	NOTE: The file is not the standard LibSVM format. The number of training examples and features must be provided in the first line
	e.g. m=1605, n=123 here:
	
	1605 123
	1  1:0.4 2:0.184 8:0.5 ....
	-1 1:0.5....
	......	
	*/
	void parseDat(String file) throws IOException{ 
 
		Scanner sc= new Scanner(new FileReader(file));
		String[] line=sc.nextLine().split(" ");
		m=Integer.parseInt(line[0]); 
		n=Integer.parseInt(line[1]);
		
		Y=new int[m];
		_X=new FlexCompRowMatrix(m,n);
		
		for(int i=0; sc.hasNextLine(); i++){ 
			
			line=sc.nextLine().split("[ :]");
			Y[i]=Integer.parseInt(line[0].replace("+",""));
			for(int j=1; j<line.length; j+=2)
				_X.set(i,Integer.parseInt(line[j])-1,Double.parseDouble(line[j+1]));
		}
		sc.close();
	}
	
	//pick ij random, just a random heuristic to pick alpha_i and alpha_j. not used. 
	Point pick_ij0(){
		Random random = new Random();
		int i=random.nextInt(m);
		int j=random.nextInt(m);
		while (j==i)
			j=random.nextInt(m);
		
		return new Point(i,j);
	}
	
	// to check if x (index of alpha) belongs to I(up). 
	boolean in_Iup(int x){
		if( (alpha[x]>_a[x] && alpha[x]<_b[x]) || (Y[x]>0 && alpha[x]==_a[x]) || (Y[x]<0 && alpha[x]==_b[x]))
			return true;
		return false;
	}
	
	// to check if x (index of alpha) belongs to I(low).
	boolean in_Ilow(int x){
		if((alpha[x]>_a[x] && alpha[x]<_b[x]) || (Y[x]>0 && alpha[x]==_b[x]) || (Y[x]<0 && alpha[x]==_a[x]))
			return true;
		return false;
	}
	
	/*
	pick maximally violating pair (WSS 1: Using first order information)
	Returns a point object with the two indices of the pair of alphas
	<Stable>
	*/
	Point pick_ij1(){
		
		int i=0,j=0;
		double min_F=Double.POSITIVE_INFINITY, max_F=-Double.POSITIVE_INFINITY;
		for(int x=0; x<m; x++){
		
			double Fx=D[x]*Y[x];
			if (in_Iup(x) && Fx<min_F){ //finding the minimum F from I(up)
				min_F=Fx;
				i=x;
			}
			if(in_Ilow(x) && Fx>max_F){ //finding the maximum F from I(low)
				max_F=Fx;
				j=x; 
			}
		}
		return new Point(i,j);
	}
	
	/*pick ij: using second order information
	Don't use, not stable
	*/	
	Point pick_ij2(){
		
		int i=0,j=0;
		double min_F=Double.POSITIVE_INFINITY, max_F=-Double.POSITIVE_INFINITY;
		for(int x=0; x<m; x++){
			double Fx=D[x]*Y[x];
			if (in_Iup(x) && Fx<min_F){
				min_F=Fx;
				i=x;
			}
		}

		double[] Qi=cache.getUnchecked(i);
		
		for(int x=0; x<m; x++){
			
			double eta=Qi[i]+Qdiag[x]+Qi[x];
			double sigma=Y[i]*Y[x]*D[i]-D[x];
			
			if(eta<=0)
				eta=tau*tau*tau;
			
			double Fx=sigma*sigma/eta;
			if(in_Ilow(x) && Fx>max_F){
				max_F=Fx;
				j=x;
			}
		}
		
		return new Point(i,j);
	}


	/*
	following method is used to update the values of alpha
	*/
	public void update(){
	
		long num_passes=1000000, passes=0;
		int i=0, j=0;
		while (passes<num_passes){

			passes+=1;
			
			//Pick i and j, use smart heuristics please
			Point ij=pick_ij1();  
			i=ij.x; j=ij.y;

			if(D[j]/Y[j]-D[i]/Y[i]<=tau) //break statement in case of convergence. 
				break;
				
			//retrieve columns from cache
			double[] Qi=cache.getUnchecked(i), Qj=cache.getUnchecked(j); 


			int s=Y[i]*Y[j];
			//check for maxima
			double eta=Qi[i]+Qj[j]-2*s*Qi[j];
			if (eta<=0)
				continue;	
				
				
			double g=alpha[i]+s*alpha[j];
			double _alpha_i=alpha[i]; double _alpha_j=alpha[j];	
			
			//establish bounds
			double L, H;
			if (s==1){
				L = Math.max(_a[i],g-_b[j]);
				H = Math.min(_b[j],g-_a[i]);
			}
			else{
				L = Math.max(_a[j],_a[i]-g);
				H = Math.min(_b[j],_b[i]-g);
			}
			if (L == H)
				continue;
		
			//update alpha[j]
			alpha[j]+=(-D[j]+s*D[i])/eta;

			//clip alpha[j]
			alpha[j] = Math.min(H, alpha[j]);
			alpha[j] = Math.max(L, alpha[j]);

			//update alpha[i]
			alpha[i]=g-s*(alpha[j]);
			
			//update D
			for(int x=0; x<m; x++)
				D[x]+=(alpha[i]-_alpha_i)*Qi[x]+(alpha[j]-_alpha_j)*Qj[x];
			
		}
		
		Fmin=Double.POSITIVE_INFINITY; Fmax=-Double.POSITIVE_INFINITY;
		for(int x=0; x<m; x++){ //this for loop is invoked to get the value of b. 
			double Fx=D[x]*Y[x];
			if (in_Iup(x) && Fx<Fmin)
				Fmin=Fx;
			if(in_Ilow(x) && Fx>Fmax)
				Fmax=Fx; 
		}
		//System.err.println("#iters: "+passes);
		b=(Fmax+Fmin)/2;
	
	}
	
	//this is done to reset all variables of the GSMO instance to prepare it for re-training
	public void refresh(double[] p){
	
		alpha=new double[m];
		D=new double[m];
		for(int i=0; i<m; i++){
			D[i]=p[i];
		}
		b=0;
	}
	
	//this method is called from Driver class to compute the value of b. 
	public double[] get_minmax_F(){ 
		double[] ret=new double[2];
		ret[0]=Fmin;
		ret[1]=Fmax;
		return ret;
	}
	
	public double get_b(){
		return b;
	}
	
	public double[] get_alpha(){
		return alpha;
	}
	
	//the W values are computed from the equation W = summation (alpha_i*X_i*Y_*)
	public double[] get_W(){ 
		DenseVector W=new DenseVector(n);
		
		for(int i=0; i<m; i++)
			W.add(Y[i]*alpha[i],_X.getRow(i));
		
		return W.getData();
	
	}
	
}