package com.btp.admm;

import no.uib.cipr.matrix.sparse.*;

/*This is simply an interface, nothing to edit here
This is to ensure that the kernel passed to the GSMO module
knows how to compute it's kernel value*/
public class Kernel { 
	
	private Double rho;
	
	public Kernel(Double rho)
	{
		this.rho = rho;
	}
    public double getKerVal(SparseVector Xi, SparseVector Xj, int Yi, int Yj){
  			return Yi*Yj*Xi.dot(Xj)/rho;
  		}
}