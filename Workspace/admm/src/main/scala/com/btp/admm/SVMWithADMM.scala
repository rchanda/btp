package com.btp.admm

import org.apache.spark.annotation.Experimental
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.regression.GeneralizedLinearAlgorithm
import org.apache.spark.mllib.regression.GeneralizedLinearModel
import org.apache.spark.SparkContext
import no.uib.cipr.matrix.sparse.SparseVector
import org.apache.spark.storage.StorageLevel
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix
import scala.collection.mutable.ListBuffer


class SVMADMMModel (
    val weights: SparseVector,
    val intercept: Double)extends  Serializable {

  private var threshold: Option[Double] = Some(0.0)
  
  
  def predict(testData: RDD[Node]): RDD[(Double,Double)] = {
    // A small optimization to avoid serializing the entire model. Only the weightsMatrix
    // and intercept is needed.
    val localWeights = weights
    val bcWeights = testData.context.broadcast(localWeights)
    val localIntercept = intercept
    
    testData.mapPartitionsWithIndex{ (index,iter) =>
      val w = bcWeights.value
      iter.map(v => (predictPoint(iter.next.X.getRow(index),w,localIntercept),iter.next.Y(index)))
    }
    
  }
  
  def predictPoint(
	  dataMatrix: SparseVector,
	  weightMatrix: SparseVector,
	  intercept: Double) = {
	  	val margin = weightMatrix.dot(dataMatrix) + intercept
	  	threshold match {
	  		case Some(t) => if (margin < t) 0.0 else 1.0
	  		case None => margin
	  	}
	 }
  }

class Node ( 
    private val numExamples: Int,
	private val numFeatures: Int,
	val X:FlexCompRowMatrix,
	val Y:Array[Int]) extends Serializable {

    
	var gamma = new SparseVector(numFeatures);
	var Z = new SparseVector(numFeatures);
	var W = new SparseVector(numFeatures);
	var U = new SparseVector(numFeatures);
	var p = new SparseVector(numExamples);
	val kernel = new Kernel(0.01)
	
	var g = new GSMO(X,Y,numExamples,numFeatures, 0, 1,p.getData(),0.001,kernel);
		
	var i = 0
				
	def set_p() = {

		val gammadot=new SparseVector(numExamples);
		
		gamma.set(1,Z);
		gamma.add(-1,U);
		X.mult(gamma,gammadot);
		
		for( i <- 0 to numExamples)
			p.set(i,Y(i)*gammadot.get(i)-1);
	}

	/*
	Retrieve alphas from updated GSMO g
	DenseVector operations to set W[i]= Y[i]*alpha[i]*X[i]/rho + Z[i] - U[i]
	*/
	def update_W() = {
		
	    val	alpha = g.get_alpha();
		W.zero();
		for(i <- 0 to numExamples)
			W.add(Y(i)*alpha(i)/0.01, X.getRow(i));
		W.add(gamma);
	}
	
	/*
	Uses the Z, U values from parameters to re-optimize the GSMO instance
	(GSMO objective is a function of p, which is a function of Z,U)
	Returns updated weight values
	*/	
	def talk(Z:SparseVector,U:SparseVector):SparseVector = {
		this.Z=Z;
		this.U=U;
		
		set_p();
		
		//put the newly computed p value into the GSMO object and retrain
		g.refresh(p.getData());	g.update();
		
		update_W();
		return W;
	}
}

class SVMWithADMM private (
		private val numIterations: Int,
		private val lambda:Double,
		private val rho:Double) extends Serializable {

	def this() = this(2,0.01,0.1)

	protected def createModel(weights: SparseVector, intercept: Double) = {
		new SVMADMMModel(weights, intercept)
	}

	def run(input: RDD[Node],numFeatures:Int) = {
	    
		val optimizer = new ADMMOptimizer()
			.setNumIterations(numIterations)
			.setLambda(lambda)
			.setRho(rho)
			
		val weights = optimizer.optimize(input,numFeatures)

		val intercept = 0.0
		createModel(weights,intercept)

	}
}


object SVMWithADMM {
   
    val numFeatures = 123
    def convertRDDtoNode (iter:Iterator[String]):Iterator[Node] = {
    	
    	println(iter.length)
        val _Y = new ListBuffer[Int]
	    val _X = new ListBuffer[SparseVector]
	    var num = 0
	    var i = 0
	    
        while(iter.hasNext)
	    {
	    	val items = iter.next.split(' ')
	    	val label = items.head.toDouble
	    	val (indices, values) = items.tail.map { item =>
	    	val indexAndValue = item.split(':')
	    	val index = indexAndValue(0).toInt - 1 // Convert 1-based indices to 0-based.
	    	val value = indexAndValue(1).toDouble
	    	(index, value)
	    	}.unzip
	    	
	    	
	    	_X :+ (new SparseVector(numFeatures,indices.toArray,values.toArray))
	    	_Y :+ label
	    	num = num +1
	    }
        
        val X_part=new FlexCompRowMatrix(num,numFeatures);
	
        for(i <- 0 until _X.size){
				X_part.setRow(i,_X(i))
			}
        
        var it = Iterator
        return it.apply(new Node(num,numFeatures,X_part,_Y.toArray))
    				  
    }
    
    
    def loadLibSVMFiletoNodes(
	      sc: SparkContext,
	      path: String,
	      minPartitions: Int): RDD[Node] = {
    		  val parsed = sc.textFile(path, minPartitions)
    				  .map(_.trim)
    				  .filter(line => !(line.isEmpty || line.startsWith("#")))
    				  .mapPartitions(convertRDDtoNode)
    				  parsed
    		}

    
	def train(
			input: RDD[Node],
			numIterations: Int,
			lambda:Double,
			rho:Double): SVMADMMModel = {
			new SVMWithADMM(numIterations,lambda,rho).run(input,numFeatures)
	}

    def train(
			input: RDD[Node]): SVMADMMModel = {
			new SVMWithADMM().run(input,numFeatures)
	}

}