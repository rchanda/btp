package com.btp.admm

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.AccumulableParam
import org.apache.spark.mllib.regression.LabeledPoint

import breeze.linalg.SparseVector
import breeze.linalg.DenseVector
import org.apache.spark.mllib.linalg.Vectors
import breeze.linalg.axpy

object VectorAccumulableParam extends AccumulableParam[List[Int],Int] {

	def zero(initialValue: List[Int]): List[Int] = {
			List.fill(initialValue.length)(0)
	}

	def addInPlace(r1: List[Int], r2: List[Int]): List[Int] = {
			val res = r1 ++ r2
					println("addInPlace "+r1+" "+r2+" "+res)
			return res
	}

	def addAccumulator(r: List[Int],t: Int):List[Int] = {
			val res = r :+  t
					println("addAccum "+r+" "+t+" "+res)
			return res
	}

}


object Testing {

	def main(args: Array[String]) {

		val D: Int = 2 //Total number of features of input data points
		val L: Int = 3 //Number of data points in each partition
		val N: Int = 9 //Total number of data points
		val rho: Double = 1

		val conf = new SparkConf().setMaster("local[4]").setAppName("Simple Application")
		val sc = new SparkContext(conf)

		val initialValue = List.fill(0)(0)
		val WsAccumulator = sc.accumulable(initialValue)(VectorAccumulableParam)
	
		// Map function
		def node_process(index:Int,iter: Iterator[LabeledPoint]): Iterator[LabeledPoint] = {
	
			iter.next().features.toArray
			WsAccumulator.add(index)
	
			return iter
		}
	
		val input = MLUtils.loadLibSVMFile(sc,"/home/chanda/examples/sample_libsvm_data.txt")

		val array = sc.parallelize(input.collect,L).mapPartitionsWithIndex(node_process)

		array.foreach(print)

		println("Accumulator Length "+WsAccumulator.value.length)

		WsAccumulator.value.foreach(println)

	  
		val sp1 = SparseVector.zeros[Int](10)

		val x = DenseVector.zeros[Double](5)
		val y = x :+=(x)
		
		val sp = new SparseVector(Array(0, 2),Array(1.0, 3.0),3)

		val v = Vectors.sparse(3, Array(0, 2), Array(1.0, 3.0))
		
		//axpy(1.9,v,v)
		
		
		
	}
}