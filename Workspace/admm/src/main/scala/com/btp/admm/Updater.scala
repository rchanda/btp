package org.apache.spark.mllib.optimization

import breeze.linalg.{norm => brzNorm, axpy => brzAxpy, Vector => BV}

import org.apache.spark.annotation.DeveloperApi
import org.apache.spark.mllib.linalg.{Vectors, Vector}

class Updater extends Serializable{
	def compute(
      U: Array[Array[Double]],
      Z: Vector,
      iter: Int
      ):Vector = {
	  Z
  }
}